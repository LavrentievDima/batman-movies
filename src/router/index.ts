import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import MovieView from '../views/MovieView.vue';
import Page404 from '../views/Page404.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/movies/:movieId',
    name: 'MovieView',
    component: MovieView,
  },
  {
    path: '*',
    name: 'Page404',
    component: Page404,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
