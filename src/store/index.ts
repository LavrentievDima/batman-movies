import Vue from 'vue';
import Vuex from 'vuex';
import MovieService from '@/services/MovieService';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {
      name: 'Bob Razowski',
      apiToken: '273b9080',
    },
    movies: {
      list: [],
      page: 1,
      numberOfResult: 0,
      filter: '',
    },
  },
  mutations: {
    setMovies(state, paylod) { state.movies.list = paylod; },
    setNumberOfResult(state, paylod) { state.movies.numberOfResult = paylod; },
    setFilter(state, paylod) { state.movies.filter = paylod; },
    addPage(state) { state.movies.page += 1; },
  },
  getters: {
    user: (state) => state.user,
    movies: (state) => state.movies.list
      .filter((el) => (state.movies.filter ? state.movies.filter === el.Type : true)),
    numberOfResult: (state) => state.movies.numberOfResult,
    getFilter: (state) => state.movies.filter,
  },
  actions: {
    async getMovieList({ commit, state }) {
      if (state.movies.list.length === 0) {
        const { result, numberOfResult } = await MovieService.movieService
          .getMovieList(state.user.apiToken, state.movies.page);
        commit('setMovies', result);
        commit('setNumberOfResult', numberOfResult);
        commit('addPage');
      }
    },
    async loadMore({ commit, state }) {
      const { result, numberOfResult } = await MovieService.movieService
        .getMovieList(state.user.apiToken, state.movies.page);
      commit('setMovies', [...state.movies.list, ...result]);
      commit('setNumberOfResult', numberOfResult);
      commit('addPage');
    },
  },
});
